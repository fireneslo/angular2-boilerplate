import { Component } from '@angular/core';
import template from './<%= basename %>.jade'
import style from './<%= basename %>.styl'

@Component({
  selector: '<%= selector %>',
  template: template(),
  styles: [ style ]
})
export class <%= component %>Component {

}
