import rename from 'gulp-rename'
import conflict from 'gulp-conflict'
import template from 'gulp-template'

gulp.task('component', done => {
  var dirname = ['client', 'components', config.selector].join('/')
  var basename = [config.selector, 'component'].join('.')
  var options = Object.assign({dirname, basename}, config)

  return gulp.src(config.template('component'))
    .pipe(rename({basename}))
    .pipe(template(options))
    .pipe(conflict(dirname))
    .pipe(gulp.dest(dirname))
})
