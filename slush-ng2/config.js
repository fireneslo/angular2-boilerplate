import path from 'path'
import gulp from 'gulp'
import  _ from 'lodash'

var input = process.argv.slice(3).join(' ')
var selector = _.kebabCase(input)
var property = _.camelCase(input)
var component = _.upperFirst(property)

global._ = _
global.gulp = gulp
global.config = {
  input: input,
  selector: selector,
  property: property,
  component: component,
  templates: path.join(__dirname, 'templates'),
  template(name, exts=['js', 'styl', 'jade']) {
    return path.join(this.templates, name, `**/*.{${exts}}`)
  }
}
