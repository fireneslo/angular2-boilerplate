import { RouteConfig } from '@angular/router-deprecated';
import { Component, ViewEncapsulation } from '@angular/core';
import { ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated'

import { LoginComponent } from '../login/login.component'
import { LayoutComponent } from '../layout/layout.component'
import { ProfileComponent } from '../profile/profile.component'

import template from './application.component.jade'
import style from './application.component.styl'

import {name} from '../../../package.json'

@Component({
  selector: 'body',
  template: template(),
  encapsulation: [ ViewEncapsulation.None ],
  directives: [ ROUTER_DIRECTIVES ],
  providers: [ ROUTER_PROVIDERS ],
  styles: [ style ]
})
@RouteConfig([{
  path: '/**',
  redirectTo: [ 'Login' ]
},{
  path: '/...',
  name: 'Layout',
  component: LayoutComponent
},{
  path: '/',
  name: 'Login',
  component: LoginComponent
}])
export class AppComponent {

}
