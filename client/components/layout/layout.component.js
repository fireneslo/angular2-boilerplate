import { Component } from '@angular/core';
import { RouteConfig, CanActivate, Router } from '@angular/router-deprecated'
import { ROUTER_DIRECTIVES } from '@angular/router-deprecated'
import { ProfileComponent } from '../profile/profile.component'

import template from './layout.component.jade'
import style from './layout.component.styl'

@Component({
  selector: 'layout',
  template: template(),
  styles: [ style ],
  directives: [ ROUTER_DIRECTIVES ]
})
@RouteConfig([{
  path: '/profile',
  name: 'Profile',
  component: ProfileComponent,
  useAsDefault: true
}])
@CanActivate(() => !!localStorage.authToken)
export class LayoutComponent {
  constructor(router: Router) {
    this.router = router
  }
  logout() {
    delete localStorage.authToken
    this.router.navigate(['Login'])
  }
}
