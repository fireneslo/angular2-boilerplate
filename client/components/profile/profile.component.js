import { Component } from '@angular/core';
import template from './profile.component.jade'
import style from './profile.component.styl'

@Component({
  selector: 'profile',
  template: template(),
  styles: [ style ]
})
export class ProfileComponent {
  constructor() {
    this.items = []
    function asyncChange() {
      this.items = [ "it", "works" ]
    }
    setTimeout(this::asyncChange, 500)
  }
}
