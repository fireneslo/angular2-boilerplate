import { Router } from '@angular/router-deprecated';
import { Component } from '@angular/core';
import template from './login.component.jade'

import style from './login.component.styl'

@Component({
  selector: 'login',
  template: template(),
  styles: [ style ]
})
export class LoginComponent {
  constructor(router: Router) {
    this.user = {}
    if(localStorage.authToken) {
      router.navigate([ 'Layout' ])
    }
  }
  login(user) {
    localStorage.authToken = `fake.${btoa(JSON.stringify(user))}.token`
    return router.navigate([ 'Layout' ])
  }
}
