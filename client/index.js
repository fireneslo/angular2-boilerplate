import './polyfill'

import { bootstrap } from '@angular/platform-browser-dynamic'
import { AppComponent } from './components/application/application.component'

bootstrap(AppComponent)
